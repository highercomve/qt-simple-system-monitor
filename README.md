## Building

To build this platform for pantavisor follow the following steps on a machine matching
the architecture you want to build for.

For example:

```
./mkdocker.sh
```
- Creates the Dockerfile for all supported arch

```
docker build -t qt -f Dockerfile.arm64v8 .
```
- The resultant will create docker images for qt 


